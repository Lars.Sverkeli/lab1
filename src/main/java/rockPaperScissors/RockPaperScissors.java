package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    String startPrompt = "Your choice (Rock/Paper/Scissors)?";
    String endPrompt = "Do you wish to continue playing? (y/n)?";
    
    public void run() {
        while(true) {
            System.out.println("Let's play round " + Integer.toString(roundCounter));
            String player = validate();
            Random rand = new Random();
            String computer = rpsChoices.get(rand.nextInt(rpsChoices.size()));
            String result = whoWon(player, computer);
            if (result == "c"){
                computerScore ++;
                System.out.println("Human chose " + player + ", computer chose " + computer + ". Computer wins!");
            }
            else if (result == "p"){
                humanScore ++;
                System.out.println("Human chose " + player + ", computer chose " + computer + ". Human wins!");
            }
            else if (result == "d"){
                System.out.println("Human chose " + player + ", computer chose " + computer + ". It's a tie!");
            }
            System.out.println("Score: human " + Integer.toString(humanScore) + ", computer " + Integer.toString(computerScore));
            roundCounter ++;
            String cnt = "";
            while (true) {
                cnt = readInput(endPrompt);
                if (cnt.equals("y") || cnt.equals("n")){
                    break;
                }
                else {
                    System.out.println("Please try again");
                }
            }
            if (cnt.equals("y")){
                continue;
            } else if (cnt.equals("n")){
                System.out.println("Bye bye :)");
                break;
            } else {

            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }

    public String whoWon(String player, String computer) {
        String res = "c";
        if (player.equals(computer)) {
            res = "d";
        }
        if (player.equals("rock")) {
            if (computer.equals("scissors")) {
                res = "p";
            }
        }
        if (player.equals("paper")) {
            if (computer.equals("rock")) {
                res = "p";
            }
        }
        if (player.equals("scissors")) {
            if (computer.equals("paper")) {
                res = "p";
            }
        }
        return res;
    }

    public String validate(){
        String input = "";
        while (true){
            input = readInput(startPrompt).toLowerCase();
            if (rpsChoices.contains(input)){
                break;
            }
            else {
                System.out.println("I do not understand " + input + ". Could you try again?");
            }
        }
        return input;
    }
}
